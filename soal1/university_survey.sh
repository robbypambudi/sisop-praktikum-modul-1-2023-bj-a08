#!/bin/bash

# 1a: Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
awk -F ',' '$4 == "Japan" {print $1 "|" $2 "|" $4}' 2023\ QS\ World\ University\ Rankings.csv | sort -k1,1 -n | head -n 5 > 1a.csv

# 2. Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.
awk -F ',' '$3 == "JP" {print}' 2023\ QS\ World\ University\ Rankings.csv \
  | sort -t "," -k9,9n | head -n 5\
  | awk -F ',' '{print $1 "|" $2 "|" $3 "|" $9}' > 1b.csv

# 3. Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
awk -F ',' '$3 == "JP" {print}' 2023\ QS\ World\ University\ Rankings.csv \
  | sort -t "," -n -k20,20 | head -n 10 \
  | awk -F ',' '{print $1 "|" $2 "|" $20}'> 1c.csv

# Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.
awk -F ',' '$2 ~ /Keren/ {print $1 "|" $2 "|" $3}' 2023\ QS\ World\ University\ Rankings.csv > 1d.csv



