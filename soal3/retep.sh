#!/bin/bash
#masukkan username dan password
echo "Input Your Username :"
read username
echo "Input Your Password :"
read -s password

#mengecek apakah username dan password sudah sesuai dengan akun yang terdaftar
if grep -q "^$username $password$" users/users.txt; then
	echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >>./log.txt
	echo "Login Success"
else
	echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >>./log.txt
	echo "Login Failed"
fi

