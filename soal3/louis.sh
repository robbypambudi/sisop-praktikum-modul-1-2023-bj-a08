#!/bin/bash
#membuat direktori users dan file users.txt
if [ -e users ]; then
	cd users
	touch users.txt
	cd ..
else
	mkdir users
	cd users
	touch users.txt
	cd ..
fi

#inputkan username
echo "Input Username :"
read username

#mengecek apakah username sudah ada atau belum
while grep -q "^${username}" ./users/users.txt; do
	echo "$(date +"%y/%m/%d %T") REGISTER: ERROR User already exists">>./log.txt
	echo "Username already exists, input new username"
	read username
done

echo "Input Password :"
while [ true ]; do 

	#input password
	read -s password

	if [[ "$password" == "$username" ]]; then
					echo "Password must not be the same as username, input your new password :"
		continue
	fi
	#mengecek password agar tidak kurang dari 8 karakter
	if [[ ${#password} -lt 8 ]]; then
					echo "Password must be at least 8 characters long, input your new password :"
		continue
	fi
	#mengecek password agar mengandung huruf kecil
	if [[ ! "$password" =~ [a-z] ]]; then
					echo "Password must contain at least one lowercase letter, input your new password :"
		continue
	fi
	#mengecek password agar mengandung huruf kapital
	if [[ ! "$password" =~ [A-Z] ]]; then
					echo "Password must contain at least one uppercase letter, input your new password :"
		continue
	fi
	#mengecek password agar mengandung angka
	if [[ ! "$password" =~ [0-9] ]]; then
					echo "Password must contain at least one number, input your new password :"
		continue
	fi
	#mengecek password agar tidak mengandung kata chicken
	if [[ "$password" =~ [Cc][Hh][Ii][Cc][Kk][Ee][Nn] ]]; then
					echo "Password must not contain the word 'chicken', input your new password : "
		continue
	fi
	#mengecek password agar tidak mengandung kata ernie
	if [[ "$password" =~ [Ee][Rr][Nn][Ii][Ee] ]]; then
					echo "Password must not contain the word 'ernie', input your new password :"
		continue
	fi
	break
done
#memasukkan info register ke file log.txt
echo "$(date +"%y/%m/%d %T") REGISTER: INFO User $username registered successfully" >> ./log.txt
echo "Account Registered Successfully"
#mengatur path agar menuju direktori users dan file users.txt
path_user=./users/users.txt
#memasukkan username dan password ke file users.txt pada direktori users
echo $username $password >> "$path_user"
