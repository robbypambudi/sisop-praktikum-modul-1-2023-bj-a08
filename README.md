# Kelompok A08 Sistem Operasi 2023

- [Kelompok A08 Sistem Operasi 2023](#kelompok-a08-sistem-operasi-2023)
  - [Problem 1](#problem-1)
    - [Point A](#point-a)
      - [Deskripsi Problem](#deskripsi-problem)
      - [Solusi](#solusi)
      - [Code](#code)
      - [Output](#output)
    - [Point B](#point-b)
      - [Deskripsi Problem](#deskripsi-problem-1)
      - [Solusi](#solusi-1)
      - [Code](#code-1)
      - [Output](#output-1)
    - [Point C](#point-c)
      - [Deskripsi Problem](#deskripsi-problem-2)
      - [Solusi](#solusi-2)
      - [Code](#code-2)
      - [Output](#output-2)
    - [Point D](#point-d)
      - [Deksripsi Problem](#deksripsi-problem)
      - [Solusi](#solusi-3)
      - [Code](#code-3)
      - [Output](#output-3)
  - [Problem 2](#problem-2)
    - [Point A](#point-a-1)
      - [Deskripsi Problem](#deskripsi-problem-3)
      - [Solusi](#solusi-4)
      - [Cron Job](#cron-job)
    - [Point B](#point-b-1)
      - [Deskripsi Problem](#deskripsi-problem-4)
      - [Solusi](#solusi-5)
      - [Cron Job](#cron-job-1)
  - [Problem 3](#problem-3)
    - [Point A](#point-a-2)
      - [Deskripsi Problem](#deskripsi-problem-5)
      - [Solusi](#solusi-6)
      - [Code](#code-4)
      - [Output](#output-4)
    - [Point B](#point-b-2)
      - [Deskripsi Problem](#deskripsi-problem-6)
      - [Solusi](#solusi-7)
      - [Code](#code-5)
      - [Output](#output-5)
    - [Dokumentasi Error Ketika Pengerjaan](#dokumentasi-error-ketika-pengerjaan)
  - [Problem 4](#problem-4)
    - [Point A](#point-a-3)
      - [Deskripsi Problem](#deskripsi-problem-7)
      - [Solusi](#solusi-8)
      - [Code](#code-6)
      - [Output](#output-6)
    - [Point B](#point-b-3)
      - [Deskripsi Problem](#deskripsi-problem-8)
      - [Solusi](#solusi-9)
      - [Code](#code-7)
      - [Output](#output-7)
    - [Point C](#point-c-1)
      - [Deskripsi Problem](#deskripsi-problem-9)
      - [Solusi](#solusi-10)
      - [Code](#code-8)
      - [Output](#output-8)
    - [Point D](#point-d-1)
      - [Deskripsi Problem](#deskripsi-problem-10)
      - [Solusi](#solusi-11)
      - [Code](#code-9)
      - [Output](#output-9)
    - [Dokumentasi Error Ketika Pengerjaan](#dokumentasi-error-ketika-pengerjaan-1)

Kelompok A08 :
| Nama | NRP |
| ---------------------- | ---------- |
| Robby Ulung Pambudi | 5025211042 |
| Mohammad Zhafran Dzaky | 5025211142 |
| Ihsan Widagdo | 5025211231 |

---

## Problem 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi :

### Point A

#### Deskripsi Problem

Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

#### Solusi

Untuk menyelesaikan problem tersebut, Bocchi perlu melakukan filter terhadap data yang ada.
Bocchi memfilter dengan mengunakan kata kunci `Japan` pada kolom `location` dan mengurutkan data dari yang terbesar ke terkecil berdasarkan kolom `rank`. Kemudian, Bocchi memilih 5 data teratas dari hasil filter tersebut.

#### Code

Implementasi dari problem tersebut adalah sebagai berikut :

```bash
awk -F ',' '$4 == "Japan" {print $1 "|" $2 "|" $4}' 2023\ QS\ World\ University\ Rankings.csv\
 | sort -k1,1 -n | head -n 5 > 1a.csv
```

**Penjelasan** </br>

1. `awk -F` : Menentukan delimiter yang digunakan pada kasus ini adalah `,`.</br>
2. `'$4 == "Japan"'` : Untuk memfilter data yang memiliki nilai pada kolom ke-4 adalah `Japan`.</br>
3. `{print $1 "|" $2 "|" $4}` : Untuk menampilkan data yang telah difilter.</br>
4. `2023\ QS\ World\ University\ Rankings.csv` : Menentukan file yang akan difilter.</br>
5. `| sort -k1,1 -n` : Untuk mengurutkan data berdasarkan kolom ke-1 secara ascending.</br>
6. `| head -n 5` : Untuk memilih 5 data teratas dari hasil filter.</br>
7. `> 1a.csv` : Untuk menyimpan hasil filter ke dalam file `1a.csv`.</br>

#### Output

```
23|The University of Tokyo|Japan
36|Kyoto University|Japan
55|Tokyo Institute of Technology (Tokyo Tech)|Japan
68|Osaka University|Japan
79|Tohoku University|Japan
```

### Point B

#### Deskripsi Problem

Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.

#### Solusi

Penyelesaian dari problem tersebut mirip dengan **point a** tetapi terdapat sedikit tambahan pada akhir kode dari **point a**. Tambahan tersebut adalah filter berdasarkan kolom `fsr score` dan mengambil data terkecil dari hasil filter point a.

#### Code

```bash
awk -F ',' '$3 == "JP" {print}' 2023\ QS\ World\ University\ Rankings.csv \
  | sort -t "," -k9,9n | head -n 5\
  | awk -F ',' '{print $1 "|" $2 "|" $3 "|" $9}' > 1b.csv

```

**Penjelasan** <br>

1. `sort -t "," -k9,9n` : Untuk mengurutkan data berdasarkan kolom ke-9 secara ascending.</br>
2. `head -n 1` : Untuk memilih 1 data teratas dari hasil filter.</br>
3. `awk -F ',' '{print $1 "|" $2 "|" $3 "|" $9}'` : Untuk menampilkan data yang telah difilter ke-2 kalinya.</br>

#### Output

```
68|Osaka University|JP|67.4
```

### Point C

#### Deskripsi Problem

Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

#### Solusi

Untuk menyelesaikan problem tersebut, Bocchi perlu melakukan filter terlebih dahulu. Bocchi memfilter dengan mengunakan kata kunci `JP` pada kolom `location code` dan mengurutkan data dari yang terbesar ke terkecil berdasarkan kolom `ger rank`. Kemudian, Bocchi memilih 10 data teratas dari hasil filter tersebut.

#### Code

```bash
awk -F ',' '$3 == "JP" {print}' 2023\ QS\ World\ University\ Rankings.csv \
  | sort -t "," -n -k20,20 | head -n 10 \
  | awk -F ',' '{print $1 "|" $2 "|" $20}'> 1c.csv
```

**Penjelasan**

1. `awk -F ',' '$3 == "JP" {print}' 2023\ QS\ World\ University\ Rankings.csv` : Untuk memfilter data yang memiliki nilai pada kolom ke-3 adalah `JP`.</br>
2. `sort -t "," -n -k20,20 | head -n 10` : Untuk mengurutkan data berdasarkan kolom ke-20 secara ascending dan memilih 10 data teratas dari hasil filter.</br>
3. `awk -F ',' '{print $1 "|" $2 "|" $20}'` : Untuk menampilkan data yang telah difilter.</br>

#### Output

```
23|The University of Tokyo|33
197|Keio University|90
206|Waseda University|127
533|Hitotsubashi University|169
911|Tokyo University of Science|188
36|Kyoto University|201
112|Nagoya University|367
55|Tokyo Institute of Technology (Tokyo Tech)|377
843|International Christian University|379
135|Kyushu University|543
```

### Point D

#### Deksripsi Problem

Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

#### Solusi

Untuk menyelesaikan problem tersebut, Bocchi perlu melakukan filter terlebih dahulu. Bocchi memfilter dengan mengunakan kata kunci yang memiliki kata `Keren` pada kolom `institution`

#### Code

```bash
awk -F ',' '$2 ~ /Keren/ {print $1 "|" $2 "|" $3}' 2023\ QS\ World\ University\ Rankings.csv > 1d.csv
```

**Penjelasan**

1. `awk -F ',' '$2 ~ /Keren/ {print $1 "|" $2 "|" $3}' 2023\ QS\ World\ University\ Rankings.csv` : Untuk memfilter data yang memiliki nilai pada kolom ke-2 memiliki kata `Keren`.</br>

#### Output

```
711|Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren)|ID
```

## Problem 2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

### Point A

#### Deskripsi Problem

Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst)

#### Solusi

Untuk menyelesaikan tugas pertama maka pertama kita harus cek apakah folder `kumpulan` sudah dibuat atau belum, jika sudah dibuat maka kita harus mencari tau ada berapa folder yang sudah dibuat.

```bash
i=1
  while [ -d kumpulan_$i ]
  do
    i=$((i+1))
  done
```

Kita simpan nilai `i` sebagai nilai index folder terakhir. Selanjutnya adalah membuat folder baru dan membuat variabel untuk menampung waktu.

```bash
mkdir kumpulan_$i
TIME=$(date +"%H")
```

Apabila sudah maka masuk kedalam folder `kumpulan_$1` maka membuat sebuah kondisi sesuai dengan soal apabila time 00.00 maka download 1 gambar saja, maka apabila diimplementasi akan sebagai berikut

```bash
if [[ $TIME -eq 0 && $(date +"%M") -eq 0 ]]
  then
    TIME=1
fi
```

**Penjelasan**

- Jika `HOUR == 0` dan `MINUTE == 0` maka rubah variabel `TIME` menjadi 1

Selanjutnya kita hanya perlu untuk **melooping** variable `TIME` untuk mengunduh image sebanyak yang diminta.

```bash
for ((j=1; j<=$TIME; j++))
  do
    wget -O perjalanan_$j.jpg https://loremflickr.com/320/240/indonesia
  done
```

Dari script diatas akan mengunduh image yang bernama `perjalanan_$j` dan `$j` merupakan urutan dari imagenya.

Seblum script tersebut kita aplikasikan dengan cronjob maka kode diatas dibungkus dengan `fungsi` yang bernama `task1` dan cara aksesnya adalah `bash kobeni_liburan.sh 1` supaya bisa kita aplikasikan bersamaan dengan tugas `point 2`.

#### Cron Job

Untuk mengkases fungsi diatas melalui cronjob maka kita harus mendeklarasikan kapan saja waktu file tersebut akan dipangil. Karena dalam soal adalah setiap 10 jam sekali maka dalam waktu format cronjob adalah :

```c
* */10 * * *
```

Selanjutnya mendeklarasikan apa yang akan system lakukan setiap 10 jam sekali dengan cara

```c
0 */10 * * * bash ~/kobeni.liburan.sh 1
```

Selesai

### Point B

#### Deskripsi Problem

Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

#### Solusi

Pertaama mendeklrasaikan variable `loop` untuk menyimpan iterasi, selanjutnya mulai lakukan perulangan seperti berikut

```bash
 while [ -d kumpulan_$loop ]
  do
    # apabila sudah dikompres, maka skip
    if [ -f devil_$loop.zip ]
    then
      loop=$((loop+1))
      continue
    fi

    # Kompress folder
    zip -r  devil_$loop.zip kumpulan_$loop
    loop=$((loop+1))
  done
```

**Penjelasan**

- `while [ -d kumpulan_$loop ]` Lakukan perulangan apbilan menemukan folder dengan dengan nama `kumpulan_$loop`
- `if [ -f devil_$loop.zip ]` apabila folder tersebut pernah di zip maka lanjutkan untuk folder berikutnya.
- `zip -r  devil_$loop.zip kumpulan_$loop` Lakukan zip kepada folder yang dipilih dengan cara `zip -r` yang artinya zip secara **rekursif**
- `loop=$((loop+1))` Ulangi langkah diatas untuk folder berikutnya.

Agar bisa terhubung dengan tugas `Point A` maka kode diatas dibungkus dengan fungsi `task2` untuk memudahkan cronjob memangilnya.

#### Cron Job

Sesuai dengan deskripsi soal cronjob akan berjalan setiap 1 hari sekali oleh sebab itu deklarasi dari cronjob sebagai berikut

```
@daily bash ~/kobeni_liburan.sh 2
```

**Penjelasan**

- Pangil kobeni_liburan.sh dengan argumen 2 setiap 1 hari sekali.

Selesai

## Problem 3

Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

### Point A

#### Deskripsi Problem

Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut

1. Minimal `8 karakter`
2. Memiliki minimal 1 `huruf kapital` dan 1 `huruf kecil`
3. `Alphanumeric`
4. Tidak boleh sama dengan `username`
5. Tidak boleh menggunakan kata `chicken` atau `ernie`

#### Solusi

Untuk menyelesaikan problem tersebut, Peter Griffin perlu memastikan password yang diinputkan tidak menyalahi aturan pembuatan password yang telah ditentukan menggunakan `if else condition` dan perulangan `while` pada file `louis.sh`. Setelah itu pastikan pada file `louis.sh` kita juga membuat perintah agar membuat direktori baru bernama users dan membuat file users.txt pada direktori tersebut jika belum ada direktori tersebut.

#### Code

Implementasi dari problem tersebut adalah sebagai berikut :

```bash
while [ true ]; do

	#input password
	read -s password

	if [[ "$password" == "$username" ]]; then
					echo "Password must not be the same as username, input your new password :"
		continue
	fi
	#mengecek password agar tidak kurang dari 8 karakter
	if [[ ${#password} -lt 8 ]]; then
					echo "Password must be at least 8 characters long, input your new password :"
		continue
	fi
	#mengecek password agar mengandung huruf kecil
	if [[ ! "$password" =~ [a-z] ]]; then
					echo "Password must contain at least one lowercase letter, input your new password :"
		continue
	fi
	#mengecek password agar mengandung huruf kapital
	if [[ ! "$password" =~ [A-Z] ]]; then
					echo "Password must contain at least one uppercase letter, input your new password :"
		continue
	fi
	#mengecek password agar mengandung angka
	if [[ ! "$password" =~ [0-9] ]]; then
					echo "Password must contain at least one number, input your new password :"
		continue
	fi
	#mengecek password agar tidak mengandung kata chicken
	if [[ "$password" =~ [Cc][Hh][Ii][Cc][Kk][Ee][Nn] ]]; then
					echo "Password must not contain the word 'chicken', input your new password : "
		continue
	fi
	#mengecek password agar tidak mengandung kata ernie
	if [[ "$password" =~ [Ee][Rr][Nn][Ii][Ee] ]]; then
					echo "Password must not contain the word 'ernie', input your new password :"
		continue
	fi
	break
done

```

**Penjelasan** </br>

1. `-s` berfungsi agar password yang diinputkan tidak terlihat oleh user
2. `while` berfungsi untuk melakukan perulangan hingga password yang dibikin telah sesuai dengan aturan
3. `if` berfungsi untuk memberikan kondisi agar password yang dibuat sesuai dengan aturan
4. `-lt8` untuk memastikan jika password yang dimasukkan user minimal 8 karakter
5. `[A-Z]` untuk memastikan password yang dimasukkan user minimal mengandung 1 huruf kapital
6. `[a-z]` untuk memastikan password yang dimasukkan user minimal mengandung 1 huruf kecil
7. `[0-9]` untuk memastikan password yang dimasukkan user minimal mengandung 1 angka
8. `[Cc][Hh][Ii][Cc][Kk][Ee][Nn]` untuk memastikan password yang dimasukkan user tidak mengandung kata chicken baik dalam huruf kapital maupun huruf kecil
9. `[Ee][Rr][Nn][Ii][Ee]` untuk memastikan password yang dimasukkan user tidak mengandung kata ernie baik dalam huruf kapital maupun huruf kecil

#### Output

1. Ketika password tidak memenuhi aturan maka program akan terus meminta user memasukkan password hingga password yang diinputkan telah benar

```
Input Password :
Password must not contain the word 'chicken', input your new password :

```

### Point B

### Deskripsi Problem

Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.

1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists

2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully

3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME

4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

### Solusi

Untuk menyelesaikan problem tersebut, Peter Griffin perlu meng-_append_ pada file `log.txt` setiap kali dia mencoba register akun dan login akun yang telah dibuat, baik kondisinya benar maupun salah. Peter Griffin juga perlu menggunakan `grep` dan `if-else condition` untuk memastikan username dan password yang dimasukkan sudah benar serta memastikan ketika register akun yang dibuat usernamenya belum pernah ada sebelumnya

### Code

1. Implementasi untuk memastikan username yang dimasukkan belum pernah ada sebelumnya ketika sedang register adalah sebagai berikut :

```bash
grep -q "^${username}" ./users/users.txt
```

2. Implementasi untuk memastikan bahwa username dan password yang dimasukkan ketika login sudah sesuai dengan username dan password yang telah didaftarkan adalah sebagai berikut :

```bash
grep -q "^$username $password$" users/users.txt
```

3. Implementasi untuk meng-_append_ info register berhasil dilakukan adalah sebagai berikut :

```bash
echo "$(date +"%y/%m/%d %T") REGISTER: INFO User $username registered successfully" >> ./log.txt
```

4. Implementasi untuk meng-_append_ info register gagal dilakukan adalah sebagai berikut :

```bash
echo "$(date +"%y/%m/%d %T") REGISTER: ERROR User already exists">>./log.txt
```

5. Implementasi untuk meng-_append_ info login berhasil dilakukan adalah sebagai berikut :

```bash
echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >>./log.txt
```

6. Implementasi untuk meng-_append_ info login gagal dilakukan adalah sebagai berikut :

```bash
echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >>./log.txt
```

**Penjelasan** </br>

1. `grep -q` untuk mencari username dan password tanpa menampilkan hasilnya

2. `>>` untuk meng-_append_ ke file

3. `$(date +"%y/%m/%d %T")` untuk mendapatkan waktu sekarang dalam format tahun-bulan-tanggal dan waktu pada `%T`

#### Output

```
23/03/03 02:35:24 REGISTER: INFO User ihsan registered successfully
23/03/03 02:40:57 REGISTER: ERROR User already exists
23/03/03 02:44:59 REGISTER: ERROR User already exists
23/03/03 02:45:13 REGISTER: INFO User ihsam registered successfully
23/03/03 02:52:10 REGISTER: INFO User ihsak registered successfully
23/03/03 02:54:26 REGISTER: INFO User kashi registered successfully
23/03/03 02:55:44 REGISTER: INFO User dagdo registered successfully
23/03/03 03:01:09 REGISTER: INFO User ican registered successfully
23/03/03 03:02:29 REGISTER: INFO User ican registered successfully
23/03/03 03:20:28 REGISTER: INFO User ican registered successfully
23/03/03 03:21:45 REGISTER: ERROR User already exists
23/03/03 03:21:56 REGISTER: INFO User ihsan registered successfully
23/03/03 11:31:21 LOGIN: ERROR Failed login attempt on user
23/03/03 11:32:21 LOGIN: ERROR Failed login attempt on user icannn
23/03/03 11:32:53 LOGIN: ERROR Failed login attempt on user ican
23/03/03 11:33:44 LOGIN: ERROR Failed login attempt on user ican
23/03/03 11:34:26 LOGIN: ERROR Failed login attempt on user ican
23/03/03 11:40:42 LOGIN: ERROR Failed login attempt on user ican
23/03/03 11:47:06 LOGIN: ERROR Failed login attempt on user ican
23/03/03 11:47:25 LOGIN: ERROR Failed login attempt on user ican
23/03/03 11:49:20 LOGIN: ERROR Failed login attempt on user ican
23/03/03 11:53:35 LOGIN: ERROR Failed login attempt on user ican
23/03/03 12:04:50 REGISTER: INFO User dagdo registered successfully
23/03/03 12:05:01 LOGIN: ERROR Failed login attempt on user dagdo
23/03/03 12:08:44 REGISTER: INFO User dagro registered successfully
23/03/03 12:10:23 LOGIN: INFO User ican logged in
23/03/03 12:11:02 LOGIN: ERROR Failed login attempt on user ican
23/03/03 14:23:09 REGISTER: ERROR User already exists
23/03/03 14:24:39 REGISTER: ERROR User already exists
23/03/03 14:25:14 REGISTER: INFO User iksan registered successfully
23/03/03 14:26:02 REGISTER: INFO User iksann registered successfully
23/03/03 14:26:29 REGISTER: INFO User ikdo registered successfully
23/03/03 14:26:58 LOGIN: ERROR Failed login attempt on user ikd
23/03/03 14:27:08 LOGIN: INFO User ican logged in
23/03/03 14:32:40 REGISTER: ERROR User already exists
23/03/03 14:32:50 REGISTER: INFO User ikza registered successfully
23/03/03 15:18:18 REGISTER: INFO User igzan registered successfully
23/03/03 15:38:00 REGISTER: INFO User ikhsan registered successfully
23/03/03 15:39:31 REGISTER: INFO User iklon registered successfully
23/03/03 15:40:00 REGISTER: INFO User lemon registered successfully
23/03/03 15:40:20 REGISTER: INFO User lemong registered successfully

```
#### Dokumentasi Error Ketika Pengerjaan


1. [Error users.txt tidak ditemukan](https://drive.google.com/file/d/1qjtE8gdOGUxBHNWhWBiuaOpPoyVeVp_A/view?usp=sharing)

2. [Error file louis.sh tidak dapat permission](https://drive.google.com/file/d/1YICkUAFIliaQynayRqqzxR2tH28xzngh/view?usp=sharing)

## Problem 4

Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan :

### Point A

#### Deskripsi Problem

Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).

#### Solusi

Untuk menyelesaikan problem tersebut, Johan perlu mendapatkan terlebih dahulu jam, menit, tanggal, bulan, dan tahun sekarang menggunakan command `date`. Kemudian barulah panggil elemen-elemen tersebut ke dalam variabel nama file (`filename`) sesuai dengan urutan format yang diminta

#### Code

Implementasi dari problem tersebut adalah sebagai berikut :

```bash
filename=$(date "+%H":"%M %d":"%m":"%y")
```

**Penjelasan** </br>

1. `filename` adalah nama variabel yang akan digunakan untuk menyimpan format penamaan file.</br>
2. `date` adalah perintah linux untuk mendapatkan elemen - elemen waktu seperti jam, menit, tanggal, dll.</br>
3. `%H` digunakan untuk mendapatkan elemen _hour_ atau jam.</br>
4. `%M` digunakan untuk mendapatkan elemen _minute_ atau menit.</br>
5. `%d` digunakan untuk mendapatkan elemen _day_ atau hari.</br>
6. `%m` digunakan untuk mendapatkan elemen _month_ atau bulan.</br>
7. `%y` digunakan untuk mendapatkan elemen _year_ atau tahun.</br>
8. Terakhir, pisahkan elemen - elemen tersebut menggunakan tanda `:` seperti yang diminta

#### Output

```
20:19 01:03:23.txt
```

### Point B

#### Deskripsi Problem

Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:

- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
- Setelah huruf z akan kembali ke huruf a

#### Solusi

Untuk menyelesaikan problem tersebut, pertama Johan perlu mendapatkan terlebih dahulu waktu saat ini (jam) menggunakan perintah `date` dan panggil elemen `%H`. kemudian simpan kedalam variabel `hour` yang nantinya akan dipakai untuk sistem chipernya. <br>

Langkah berikutnya adalah buat 2 array yang masing - masing berisi huruf - huruf _lowercase_ dan _uppercase_ untuk mendapatkan urutan alfabetnya sebagai berikut,

```bash
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
```

Kemudian untuk menyelesaikan problem chiper nya, kita dapat menggunakan perintah `tr` dalam linux untuk men-_translate_ karakter menjadi output yang kita inginkan, seperti contoh berikut ini,

```bash
echo "halo" | tr o e #mengubah o menjadi e
output : hale
```

Sehingga kita hanya perlu men-_translate_ semua karakter dari `a-z` sebanyak `hour` ditambah dengan urutan alfabet yang dapat kita akses dari array dan tidak lupa untuk di modulo 26 (jumlah alfabet). Sebelum masuk pada implementasi pada problem aktualnya, berikut ini adalah contoh sederhana penggunaan `tr` untuk men-_translate_ semua karakter dari `a-z` menjadi `b-a` dengan syntax sebagai berikut,

```bash
tr | 'a-z' 'b-za'
```

yang akan men-_translate_ tiap huruf menjadi 1 karakter didepannya, seperti `a` menjadi `b`, `b` menjadi `c`, dan seterusnya.

Atau jika kita ingin men-_translate_ setiap karakter dengan jarak yang cukup jauh, dapat kita singkat penulisan perintah tr menjadi seperti berikut,

```bash
tr | 'a-z' 'j-za-i'
```

yang akan men-_translate_ tiap huruf menjadi 10 karakter didepannya, seperti `a` menjadi `j`, `b` menjadi `k`, dan seterusnya.

Sehingga kita hanya perlu menyimpan parameter output dari perintah `tr` tadi ke dalam sebuah variabel yang dinamakan `chiper1` untuk _lowercase_ dan `chiper2` untuk _uppercase_ sesuai jam dan urutan alfabet yang telah kita simpan sebelumnya dengan syntax seperti berikut,

```bash
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]}"
```

di mana kita dapat mengakses karakter pada array `lowercase` dan `uppercase` secara dinamis sesuai dengan variabel `hour` yang telah kita siapkan sebelumnya untuk mendapatkan output untuk parameter `tr` sesuai dengan permintaan soal.

Jangan lupa juga untuk pindah ke directory tempat kita ingin menyimpan hasil enkripsinya. Barulah kita tuliskan hasil enkripsinya ke `filename.txt` yang telah kita namai sebelumnya dengan menggunakan perintah `echo` dan operator `>`.

#### Code

Implementasi dari problem tersebut adalah sebagai berikut :

```bash
#/!bin/bash

# nama file hasil enkripsi
filename=$(date "+%H":"%M %d":"%m":"%y")

# variabel untuk mendapatkan jam sekarang
hour=$(date "+%H")

# buat array untuk mendapatkan value per hurufnya
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# chiper1(lowercase) dan chiper2(uppercase) adalah string untuk parameter tr yang menggeser barisan alfabet
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]}"

# pindah ke directory yang diinginkan untuk menyimpan hasil dekripsi
cd '/home/vron/sisop/praktikum-1/sisop-praktikum-modul-1-2023-bj-a08/soal4'

# str untuk menyimpan string hasil enkripsi
echo -n "$(cat /var/log/syslog | tr 'a-z' $cipher1 | tr 'A-Z' $cipher2)" > "$filename.txt"
```

**Penjelasan** </br>

1. `filename` adalah nama variabel yang akan digunakan untuk menyimpan format penamaan file hasil enkripsi.</br>
2. `hour` digunakan untuk menyimpan elemen jam sekarang.</br>
3. `lowercase` digunakan untuk menyimpan karakter - karakter lowercase.</br>
4. `uppercase` digunakan untuk menyimpan karakter - karakter uppercase.</br>
5. `cipher1` digunakan untuk menyimpan parameter output perintah `tr` pada karakter `lowercase`.</br>
6. `cipher2` digunakan untuk menyimpan parameter output perintah `tr` pada karakter `uppercase`.</br>
7. `cd '/home/vron/sisop/praktikum-1/nomer-4'` digunakan untuk pindah directory ke tempat hasil enkripsi disimpan.</br>
8. tuliskan hasil enkripsinya ke `$filename`.txt menggunakan perintah `echo -n` untuk tetap menjaga white space file yang akan dienkripsi dan operator `>` untuk membuatkan file baru jika belum ada.

#### Output

```
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] Hejqt ranoekj 5.19.0-32-cajaney (xqehzz@hyu02-wiz64-026) (t86_64-hejqt-cjq-cyy (Qxqjpq 11.3.0-1qxqjpq1~22.04) 11.3.0, CJQ hz (CJQ Xejqpeho bkn Qxqjpq) 2.38) #33~22.04.1-Qxqjpq OIL LNAAILP_ZUJWIEY Ikj Fwj 30 17:03:34 QPY 2 (Qxqjpq 5.19.0-32.33~22.04.1-cajaney 5.19.17)
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] Ykiiwjz heja: XKKP_EIWCA=/xkkp/rihejqv-5.19.0-32-cajaney nkkp=QQEZ=wz081b7x-axxb-41ay-w7a1-9w44b7y0x971 nk mqeap olhwod
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] GANJAH oqllknpaz ylqo:
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000]   Ejpah CajqejaEjpah
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000]   WIZ WqpdajpeyWIZ
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000]   Duckj DuckjCajqeja
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000]   Yajpwqn YajpwqnDwqho
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000]   vdwktej   Odwjcdwe
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] t86/blq: Oqllknpejc TOWRA bawpqna 0t001: 't87 bhkwpejc lkejp naceopano'
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] t86/blq: Oqllknpejc TOWRA bawpqna 0t002: 'OOA naceopano'
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] t86/blq: Oqllknpejc TOWRA bawpqna 0t004: 'WRT naceopano'
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] t86/blq: topwpa_kbboap[2]:  576, topwpa_oevao[2]:  256
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] t86/blq: Ajwxhaz topwpa bawpqnao 0t7, ykjpatp oeva eo 832 xupao, qoejc 'opwjzwnz' bkniwp.
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] oecjwh: iwt oecbnwia oeva: 1776
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-lnkrezaz lduoeywh NWI iwl:
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-a820: [iai 0t0000000000000000-0t000000000009bxbb] qowxha
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-a820: [iai 0t000000000009by00-0t000000000009bbbb] naoanraz
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-a820: [iai 0t00000000000b0000-0t00000000000bbbbb] naoanraz
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-a820: [iai 0t0000000000100000-0t00000000zbbabbbb] qowxha
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-a820: [iai 0t00000000zbbb0000-0t00000000zbbbbbbb] WYLE zwpw
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-a820: [iai 0t00000000bay00000-0t00000000bay00bbb] naoanraz
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-a820: [iai 0t00000000baa00000-0t00000000baa00bbb] naoanraz
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-a820: [iai 0t00000000bbby0000-0t00000000bbbbbbbb] naoanraz
Bax 25 08:58:56 rnkj-RenpqwhXkt ganjah: [    0.000000] XEKO-a820: [iai 0t0000000100000000-0t000000030zbbbbbb] qowxha
```

### Point C

#### Deskripsi Problem

Buat juga script untuk dekripsinya.

#### Solusi

Untuk menyelesaikan problem tersebut, pertama Johan perlu mendapatkan terlebih dahulu waktu saat ini (jam) menggunakan perintah `date` dan panggil elemen `%H`. kemudian simpan kedalam variabel `hour` yang nantinya akan dipakai untuk sistem chipernya. <br>

Langkah berikutnya adalah buat 2 array yang masing - masing berisi huruf - huruf _lowercase_ dan _uppercase_ untuk mendapatkan urutan alfabetnya sebagai berikut,

```bash
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
```

Kemudian untuk menyelesaikan problem chiper nya, kita dapat menggunakan perintah `tr` dalam linux untuk men-_translate_ karakter menjadi output yang kita inginkan, seperti contoh berikut ini,

```bash
echo "halo" | tr o e #mengubah o menjadi e
output : hale
```

Dan karena perintah soal kali ini adalah dekripsi, maka kita dapat tetap menggunakan perintah `tr` yang sama dengan hanya menukar parameter input menjadi output dan output menjadi input

Sehingga `tr` kali ini akan men-_translate_ karakter dari `cipher1` dan `cipher2` kembali ke `a-z`

```bash
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]}"
```

Jangan lupa juga untuk pindah ke directory tempat kita ingin menyimpan hasil dekripsinya. Barulah kita tuliskan hasil dekripsinya ke `filename.txt` yang telah kita namai sebelumnya dengan menggunakan perintah `echo` dan operator `>`.

#### Code

Implementasi dari problem tersebut adalah sebagai berikut :

```bash
#/!bin/bash

# buat file dekripsi dengan format nama file bebas
filename=$(date "+%H":"%M %d":"%m":"%y decrypt")

# simpan jam sekarang di variabel hour
hour=$(date "+%H")

# buat array untuk menyimpan huruf agar bisa didapat valuenya
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z )
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# chiper1(lowercase) dan chiper2(uppercase) adalah string untuk parameter tr yang menggeser barisan alfabet
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]}"

# pindah ke directory yang diinginkan untuk menyimpan hasil dekripsi
cd '/home/vron/sisop/praktikum-1/sisop-praktikum-modul-1-2023-bj-a08/soal4'

# tuliskan hasil dekripsi ke $filename.txt
echo -n "$(cat 22:54\ 02:03:23.txt | tr $cipher1 'a-z' | tr $cipher2 'A-Z')" > "$filename.txt"
```

**Penjelasan** </br>

1. `filename` adalah nama variabel yang akan digunakan untuk menyimpan format penamaan file hasil dekripsi.</br>
2. `hour` digunakan untuk menyimpan elemen jam sekarang.</br>
3. `lowercase` digunakan untuk menyimpan karakter - karakter lowercase.</br>
4. `uppercase` digunakan untuk menyimpan karakter - karakter uppercase.</br>
5. `cipher1` digunakan untuk menyimpan parameter output perintah `tr` pada karakter `lowercase`.</br>
6. `cipher2` digunakan untuk menyimpan parameter output perintah `tr` pada karakter `uppercase`.</br>
7. `cd '/home/vron/sisop/praktikum-1/nomer-4'` digunakan untuk pindah directory ke tempat hasil dekripsi disimpan.</br>
8. tuliskan hasil dekripsinya ke `$filename`.txt menggunakan perintah `echo -n` untuk tetap menjaga white space file yang akan didekripsi dan operator `>` untuk membuatkan file baru jika belum ada.

#### Output

```
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] Linux version 5.19.0-32-generic (buildd@lcy02-amd64-026) (x86_64-linux-gnu-gcc (Ubuntu 11.3.0-1ubuntu1~22.04) 11.3.0, GNU ld (GNU Binutils for Ubuntu) 2.38) #33~22.04.1-Ubuntu SMP PREEMPT_DYNAMIC Mon Jan 30 17:03:34 UTC 2 (Ubuntu 5.19.0-32.33~22.04.1-generic 5.19.17)
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] Command line: BOOT_IMAGE=/boot/vmlinuz-5.19.0-32-generic root=UUID=ad081f7b-ebbf-41ec-a7e1-9a44f7c0b971 ro quiet splash
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] KERNEL supported cpus:
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000]   Intel GenuineIntel
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000]   AMD AuthenticAMD
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000]   Hygon HygonGenuine
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000]   Centaur CentaurHauls
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000]   zhaoxin   Shanghai
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point registers'
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] x86/fpu: Supporting XSAVE feature 0x002: 'SSE registers'
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] x86/fpu: Supporting XSAVE feature 0x004: 'AVX registers'
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] x86/fpu: xstate_offset[2]:  576, xstate_sizes[2]:  256
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] x86/fpu: Enabled xstate features 0x7, context size is 832 bytes, using 'standard' format.
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] signal: max sigframe size: 1776
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-provided physical RAM map:
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-e820: [mem 0x0000000000000000-0x000000000009fbff] usable
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-e820: [mem 0x000000000009fc00-0x000000000009ffff] reserved
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-e820: [mem 0x00000000000f0000-0x00000000000fffff] reserved
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-e820: [mem 0x0000000000100000-0x00000000dffeffff] usable
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-e820: [mem 0x00000000dfff0000-0x00000000dfffffff] ACPI data
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-e820: [mem 0x00000000fec00000-0x00000000fec00fff] reserved
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-e820: [mem 0x00000000fee00000-0x00000000fee00fff] reserved
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-e820: [mem 0x00000000fffc0000-0x00000000ffffffff] reserved
Feb 25 08:58:56 vron-VirtualBox kernel: [    0.000000] BIOS-e820: [mem 0x0000000100000000-0x000000030dffffff] usable
```

### Point D

#### Deskripsi Problem

Backup file syslog setiap 2 jam untuk dikumpulkan 😀.

#### Solusi

Untuk menyelesaikan problem tersebut, Johan perlu membuat crontab baru terlebih dahulu dengan menggunakan syntax berikut ini,

```bash
crontab -e
```

crontab merupakan sebuah perintah yang memberikan izin pada user untuk mengeksekusi suatu perintah tertentu atau cronjob pada waktu tertentu. Pada point ini, kita diminta untuk mem-_backup_ file syslog setiap 2 jam untuk dikumpulkan

Hal tersebut dapat kita lakukan dengan mengatur parameter - parameter pada crontab sebagai berikut,

```
# m h dom mon dow command
* * * * * perintah yang akan dieksekusi
– – – – –
| | | | |
| | | | +—– day of week (0 – 7) (Sunday=0)
| | | +——- month (1 – 12)
| | +——— day of month (1 – 31)
| +———– hour (0 – 23)
+————- min (0 – 59)
```

dimana,

- m - Minute (menit) - 0 to 59
- h - Hour (jam) - 0 to 23
- dom - Day of Month (tanggal) - 0 to 31
- mon - Month (bulan) - 0 to 12
- dow - Day of Week (nomor hari) - 0 to 7 (0 dan 7 adalah hari minggu)

#### Code

Karena perintah soal dilakukan setiap 2 jam sekali, maka dapat kita tulis sebagai berikut,

```
0 */2 * * * /bin/bash /home/vron/sisop/praktikum-1/sisop-praktikum-modul-1-2023-bj-a08/soal4/log_encrypt.sh
```

**Penjelasan** </br>

1. `0` parameter pertama adalah menit. Karena kita butuh tepat 2 jam, maka dapat kita tulis 0.</br>
2. `*/2` parameter kedua adalah jam, dimana penulisan seperti itu berarti bahwa perintah akan dilakukan tiap 2 jam sekali.</br>
3. `* * *` parameter ketiga, empat, dan lima dapat kita isi `*` karena kita tidak butuh tanggal, bulan, atau hari tertentu untuk mengeksekusi perintah soal.</br>
4. Dan parameter terakhir adalah command serta file path dari perintah yang akan dieksekusi yang dapat disesuaikan letak filenya.</br>

#### Output

```
20:19 01:03:23.txt
```

#### Dokumentasi Error Ketika Pengerjaan


1. [Error file output (hasil enkripsi) tidak keluar](https://drive.google.com/file/d/1GkLNuZBfKhzgk8iVBDtE5lMJ5lCDxbJD/view?usp=sharing)