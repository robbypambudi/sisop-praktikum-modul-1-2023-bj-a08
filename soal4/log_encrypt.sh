#/!bin/bash

# nama file hasil enkripsi
filename=$(date "+%H":"%M %d":"%m":"%y")

# variabel untuk mendapatkan jam sekarang
hour=$(date "+%H")

# buat array untuk mendapatkan value per hurufnya
lowercase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
uppercase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# chiper1(lowercase) dan chiper2(uppercase) adalah string untuk parameter tr yang menggeser barisan alfabet 
cipher1="${lowercase[$hour]}-za-${lowercase[$hour-1]}"
cipher2="${uppercase[$hour]}-ZA-${uppercase[$hour-1]}"


# pindah ke directory yang diinginkan untuk menyimpan hasil dekripsi
cd '/home/vron/sisop/praktikum-1/sisop-praktikum-modul-1-2023-bj-a08/soal4'

# str untuk menyimpan string hasil enkripsi
echo -n "$(cat /var/log/syslog | tr 'a-z' $cipher1 | tr 'A-Z' $cipher2)" > "$filename.txt"

# 0 */2 * * * /bin/bash /home/vron/sisop/praktikum-1/sisop-praktikum-modul-1-2023-bj-a08/soal4/log_encrypt.sh
